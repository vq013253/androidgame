package reading.ac.uk.androidgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LevelSelect extends AppCompatActivity {

    private String levelFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_select);
    }

    /**
     * @usage Is invoked upon pressing the onscreen buttons and passes the tag of the selected button to load a level
     * @param view to identify the root of button
     */
    public void onClick(View view) {
        //prepare intent
        Intent launchGame = new Intent(this, Game.class);
        //add extra message of which level to load
        levelFile = ((Button) view).getTag().toString();
        launchGame.putExtra("LEVEL_NAME", levelFile);
        //start
        startActivity(launchGame);
    }

    /**
     * Method to open the MakeLevel activity
     * @param view to identify the root of button
     */
    public void makeLevel(View view){
        //prepare intent
        Intent makeLevel = new Intent(this, MakeLevel.class);
        //add extra message of which level to load
        levelFile = "user";
        makeLevel.putExtra("LEVEL_NAME", levelFile);
        //start
        startActivity(makeLevel);
    }

    /**
     * Method to terminate the LevelSelect activity
     * @param view to identify the root of button
     */
    public void exitLevelSelect(View view) {
        this.finish();
    }
}
