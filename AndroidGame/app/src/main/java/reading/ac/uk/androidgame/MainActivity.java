package reading.ac.uk.androidgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button buttonPlay, buttonExit, buttonAbout, buttonScores, buttonSettings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //getting the menu buttons
        buttonPlay = findViewById(R.id.buttonPlay);
        buttonExit = findViewById(R.id.buttonExit);
        buttonAbout = findViewById(R.id.buttonAbout);
        buttonScores = findViewById(R.id.buttonScores);
        buttonSettings = findViewById(R.id.buttonSettings);
    }

    /**
     * Method ran on button "Play" click in the main activity; Starts a new Level Select activity;
     *
     * @param view - view in which the button pressed is located;
     */
    public void startGame(View view) {
        startActivity(new Intent(this, LevelSelect.class));
    }

    /**
     * Unimplemented method ran on button "About" click in the main activity; Starts a new About activity;
     *
     * @param view - view in which the button pressed is located;
     */
    public void aboutGame(View view) {
        //startActivity(new Intent(this, .class));
    }

    /**
     * Unimplemented method ran on button "Scores" click in the main activity; Starts a new Score activity;
     *
     * @param view - view in which the button pressed is located;
     */
    public void scoresGame(View view) {
        //startActivity(new Intent(this, .class));
        /*
        public void retriveDatabaseInformation () {
            rootNode = FirebaseDatabase.getInstance("https://myapplication-b8bb7-default-rtdb.firebaseio.com/"); // store the database reference to a variable
            reference = rootNode.getReference(); // get the reference of the real-time database
            reference.child("Users").addValueEventListener(new ValueEventListener() { // create value event listener of the child ,,Users''@RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) { // use datasnapshots which holds the essential data of the Firebase database
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated
                    for (DataSnapshot ds : dataSnapshot.getChildren()) { // foreach loop over all ,,Users'' children
                        name = ds.getKey(); // store keys
                        finalScore = Integer.parseInt(ds.getValue().toString()); // store values
                        map.add(new KeysValues(name, finalScore)); // add them to the list
                    }
                    map = map.stream().sorted((x, y) -> y.score.compareTo(x.score)).collect(Collectors.toList()); // sort the values in ascending order
                    textOnScreen1 = (TextView) findViewById(R.id.textViewTest1);
                    textOnScreens.add(textOnScreen1);
                    textOnScreen2 = (TextView) findViewById(R.id.textViewTest2);
                    textOnScreens.add(textOnScreen2);
                    textOnScreen3 = (TextView) findViewById(R.id.textViewTest3);
                    textOnScreens.add(textOnScreen3);
                    textOnScreen4 = (TextView) findViewById(R.id.textViewTest4);
                    textOnScreens.add(textOnScreen4);
                    textOnScreen5 = (TextView) findViewById(R.id.textViewTest5);
                    textOnScreens.add(textOnScreen5);
                    int count = 0;
                    // iterate over the list size, set scores and usernames as a text of each text view and break the loop when it hits 5
                    for (int i = 0; i < map.size(); i++) {
                        textOnScreens.get(i).setText(i + 1 + "." + map.get(i).name + " -> " + map.get(i).score);
                        count++;
                        if (count == 5) break;


                    }
                }

            }
        } */
    }

                    /**
                     * Unimplemented method ran on button "Settings" click in the main activity; Starts a new Settings activity;
                     * @param view - view in which the button pressed is located;
                     */
            public void settingsGame (View view){
                //startActivity(new Intent(this, .class));
            }

            /**
             * Method ran on button "Exit" click in the main activity; Closes the activity;
             * @param view - view in which the button is located
             */
            public void exitGame (View view){
                this.finish();
            }
        }
