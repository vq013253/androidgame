package reading.ac.uk.androidgame;

/*
public class OnlineScoring {
    onlineScorerRecorder = new OnlineScorerRecorder(readLoudExercise, "");

        onlineScorerRecorder.setConnectTimeoutMillis(1000);
        onlineScorerRecorder.setResponseTimeoutMillis(5000);


        onlineScorerRecorder.setOnRecordStopListener(new OnlineScorerRecorder.OnRecordListener() {
        @Override
        public void onRecordStop(Throwable error, OnlineScorerRecorder.Result result) {
            if (error != null) {
                Toast.makeText(DemoActivity.this, "\n" + Log.getStackTraceString(error),
                        Toast.LENGTH_SHORT).show();
            }
            if (errorFilePath != null) {
                recordBtn.setText("retry");
            } else {
                recordBtn.setText("start");
            }
        }
    });


        onlineScorerRecorder.setOnProcessStopListener(new OnlineScorerRecorder.OnProcessStopListener() {

        @Override
        public void onProcessStop(Throwable error, String filePath, String report) {
            if (error != null) {

                if (error instanceof OnlineScorerRecorder.ScorerException) {
                    errorFilePath = "/sdcard/retry.wav";
                    boolean renameSuccess = new File(filePath).renameTo(new File(errorFilePath));
                    if (renameSuccess) {
                        resultView.setText(error.getMessage());
                        recordBtn.setText("retry");
                        fetchLogFile();
                        return;
                    }
                }
                errorFilePath = null;
                recordBtn.setText("start");
                resultView.setText(error.getMessage());
            } else {
                errorFilePath = null;

                resultView.setText(String.format("filePath = %s\n report = %s", filePath, report));
                recordBtn.setText("start");
            }
            fetchLogFile();
        }

        private void fetchLogFile() {

            OnlineScorer.requestLogDir(
                    new RequestLogCallback() {
                        @Override
                        public void onDirResponse(File logDir) {
                            if (logDir != null) {
                                final String result = String.format("%s\n logDir is %s",
                                        resultView.getText(),
                                        logDir.getAbsoluteFile());
                                resultView.setText(result);
                            }
                        }
                    });
        }
    });

        recordBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (onlineScorerRecorder.isAvailable()) {
                if (onlineScorerRecorder.isRecording()) {
                    onlineScorerRecorder.stopRecord();
                } else {
                    if (!checkRecordPermission()) return;

                    if (errorFilePath != null) {
                        onlineScorerRecorder.startRecord(errorFilePath);
                    } else {

                        onlineScorerRecorder.startRecord();
                    }
                    resultView.setText("");
                    recordBtn.setText("stop");
                }
            }
        }
    });
}

    private boolean checkRecordPermission() {
        if (PermissionChecker.checkSelfPermission(DemoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || PermissionChecker.checkSelfPermission(DemoActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                        shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
                    new AlertDialog.Builder(DemoActivity.this)
                            .setTitle(R.string.check_permission_title)
                            .setMessage(R.string.check_permission_content)
                            .setCancelable(false)
                            .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, REQUEST_CODE_PERMISSION);
                                    }
                                }
                            }).show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, REQUEST_CODE_PERMISSION);
                }
            }
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, R.string.check_permission_fail, Toast.LENGTH_LONG).show();
                    return;
                }
            }
            recordBtn.performClick();
        }
    }
}


*/