package reading.ac.uk.androidgame;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

import static java.lang.Thread.interrupted;

public class GameView extends SurfaceView implements Runnable {

    //currently used to check if running
    volatile boolean running = true;
    private Thread gThread = null;
    private Level level;
    private Game gameActivity;

    //These objects will be used for drawing
    private final Paint paint;
    private Canvas canvas;
    private final SurfaceHolder surfaceHolder;
    private final Context context;
    private final int xRes, yRes;


    //timing variables
    private long startTime;
    private long endTime;
    private long gameStartTime;
    private long gameTime;
    public final int TARGET_FPS = 60;
    public static int ACTUAL_FPS = 0;

    //the high Scores Holder
    private final int highScore[] = new int[4];

    //Shared Prefernces to store the High Scores
    SharedPreferences sharedPreferences;
    /**ddddddddddddddddddddddd**/


    /**
     * Constructor for GameView;
     * @param context - context for resources;
     * @param xRes - X resolution of the display;
     * @param yRes - Y resolution of the display;
     * @param game - Game object value to allow to call methods in Game such as showGameOver();
     */
    public GameView(Context context, int xRes, int yRes, Game game) {
        super(context);
        this.context = context;
        gameActivity = game;
        this.xRes = xRes;
        this.yRes = yRes;

        try {
            level = new Level(context, xRes, yRes, this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //initializing drawing objects
        surfaceHolder = getHolder();
        paint = new Paint();

        gameStartTime = System.currentTimeMillis();

        sharedPreferences = context.getSharedPreferences("SHAR_PREF_NAME",Context.MODE_PRIVATE);

        //initializing the array high scores with the previous values
        highScore[0] = sharedPreferences.getInt("score1",0);
        highScore[1] = sharedPreferences.getInt("score2",0);
        highScore[2] = sharedPreferences.getInt("score3",0);
        highScore[3] = sharedPreferences.getInt("score4",0);
    }


    /**Captures the touch events in the GameView and passes to Level class to handle the player movement;
     * @param motionEvent - automatically generated when touch is registered
     * @return - return value not used, motionEvent used mainly;
     */
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                level.handleTouch(false, motionEvent);
                break;
            case MotionEvent.ACTION_DOWN:
                level.handleTouch(true, motionEvent);
                break;
        }
        return true;
    }

    /**
     *The main game loop is excecuted through a Runnable to provide similar performance to a thread;
     * The method calls calculations(), draw() repeatedly and maintains the game flow;
     * @exception Exception e - thrown if the game loop is broken. Mostly from other classes;
     */
    @Override
    public void run() {
        while (running && !interrupted()) {
            try {
                synchronized (surfaceHolder) {
                    startTime = System.currentTimeMillis();

                    calculations();
                    draw();

                    endTime = System.currentTimeMillis();
                    //calculates the difference in run time
                    long delta = endTime - startTime;

                    if(delta < 1000)
                    {
                        long interval = (1000 - delta)/TARGET_FPS;
                        ACTUAL_FPS = TARGET_FPS;
                        try
                        {
                            Thread.sleep(interval); //sleep enough to maintain 60FPS
                        }
                        catch(Exception ex)
                        {}
                    }
                    else
                    {
                        ACTUAL_FPS = (int) (1000 / delta);
                    }
                }
            } catch(Exception e) {
                System.out.println("Failed loop");
                e.printStackTrace();
            }
        }
    }


    /**
     * Method that calls the level function to move the objects, perform collision detection and set Score for the GameView;
     */
    private void calculations() {
        gameActivity.setScoreText(String.valueOf(level.getScore()));

        level.movePlayer();
        level.moveEnemies();
    }

    /**
     * Method to use the canvas and surfaceHolder of GameView and pass them to Items to draw their location;
     * Handles locking and unlocking of the value;
     */
    public void draw() {
        //checking if surface is valid
        if(surfaceHolder.getSurface().isValid())
        {
            //locking the canvas
            canvas = surfaceHolder.lockCanvas();
            if(canvas != null) {
                level.drawAll(canvas, paint);
                //Unlocking the canvas
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
        canvas = null;
    }

    /**
     * Pause method for the Game Loop that handles Threads;
     * @exception InterruptedException - if unable to join the exception is thrown;
     */
    public void pause() {
        //when the game is paused
        //setting the variable to false
        running = false;
        try {
            //stopping the thread
            gThread.join();
        } catch (InterruptedException e) {
        }
    }

    /**
     * Pause method for the Game Loop that handles Threads;
     */
    public void resume() {
        //when the game is resumed
        //starting the thread again
        running = true;
        gThread = new Thread(this);
        gThread.start();
    }

    /**
     * A method to call another method in Game class to display a custom AlertDialog on UI thread to signal end of game;
     * @param type - a String value specifying the type of pop-up menu to show;
     */
    public void showPopup(String type) {
        running = false;
        switch (type){
            case "game over":
                gameActivity.showGameOver();
                break;
            case "win":
                saveScores();
                gameActivity.setScoreText(String.valueOf(level.getScore()));
                gameActivity.showWin(String.valueOf(level.getScore()));
                break;
            /**case "pause":
                gameActivity.showPause();
                break;**/
        }
    }

    /**
     * Method for getting time taken by the game loop since the start;
     * USed in final score calculation in Level class;
     * @return gameTime - total session playtime;
     */
    public long getGameTime() {
        gameTime = System.currentTimeMillis() - gameStartTime;
        return gameTime;
    }

    public String getLevelFile() {
        return gameActivity.getLevelFile();
    }

    public String getUserLevelFile() {
        return gameActivity.getUserLevelFile();
    }

    /**
     * Method to re-load the same level file in the Level class;
     * Resumes the thread after freezing with Game OverAlertDialog;
     * @throws Exception - if file not found;
     */
    public void restartLevel() throws Exception {
       running = true;
       level.loadLevel(gameActivity.getLevelFile());
       resume();
    }

    /**
     * Method to load the level file that is next in line within the Level class;
     * Resumes the thread after freezing with Level Completed AlertDialog;
     * @throws Exception - if file not found;
     */
    public void nextLevel(String fileName) throws Exception {
        running = true;
        level.loadLevel(fileName);
        resume();
    }


    /**send score from level**/
    public void saveScores(){
        int score = level.getScore();
        //Assigning the scores to the highscore integer array
        for(int i=0; i<4; i++){
            if(highScore[i]<score){

                final int finalI = i;
                highScore[i] = score;
                break;
            }
        }

        //storing the scores through shared Preferences
        SharedPreferences.Editor e = sharedPreferences.edit();
        for(int i=0; i<4; i++){
            int j = i+1;
            e.putInt("score"+j,highScore[i]);
        }
        e.apply();
    }

}

