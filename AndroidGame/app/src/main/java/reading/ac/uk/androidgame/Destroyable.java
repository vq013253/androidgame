package reading.ac.uk.androidgame;

import android.content.Context;

import static reading.ac.uk.androidgame.Item.itemRole.DESTROYABLE;

public class Destroyable extends Item {

    private int scorePenalty;
    /**
     * Constructor for Destroyable
     * @param context - resource context;
     * @param xRes - x axis limit derived from resolution;
     * @param yRes - y axis limit derived from resolution;
     * @param x - location in x axis;
     * @param y - location in y axis;
     * @param bitmapName - bitmap name inside /drawable/ without the extension;
     */
    public Destroyable(Context context, int xRes, int yRes, int x, int y, String bitmapName) {
        super(context, xRes, yRes, x, y, bitmapName);
        setItemRole(DESTROYABLE);
        scorePenalty = -700;
    }

    public int getScorePenalty() {
        return scorePenalty;
    }
}
