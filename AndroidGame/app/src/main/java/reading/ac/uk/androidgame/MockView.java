package reading.ac.uk.androidgame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MockView extends SurfaceView {

    private final Paint paint;
    private Canvas canvas;
    private final SurfaceHolder surfaceHolder;
    private final Context context;
    private final int xRes, yRes;

    /**
     * Unimplemented constructor for MockView to draw positions of already added objects
     * @param context - the resource context;
     * @param xRes - x axis limit derived from resolution;
     * @param yRes - y axis limit derived from resolution;
     */
    public MockView(Context context, int xRes, int yRes) {
        super(context);
        this.context = context;
        this.xRes = xRes;
        this.yRes = yRes;

        //initializing drawing objects
        surfaceHolder = getHolder();
        paint = new Paint();
    }

    /**
     * Method to draw a circle in the location of added game object
     * @param x - x coordinate of object
     * @param y - y coordinate of object
     */
    public void draw(float x, float y){
        if(surfaceHolder.getSurface().isValid())
        {
            //locking the canvas
            canvas = surfaceHolder.lockCanvas();
            if(canvas != null) {
                canvas.drawCircle(x, y, 30f, paint);
                //Unlocking the canvas
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
        canvas = null;

    }
}
