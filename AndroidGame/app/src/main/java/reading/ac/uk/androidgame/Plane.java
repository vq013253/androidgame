package reading.ac.uk.androidgame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import static java.lang.Math.atan2;
import static reading.ac.uk.androidgame.Item.itemRole.PLAYER;

public class Plane extends Item {

    //declare variables
    protected double speed;
    protected double momentum = 0.1; //initial momentum has to be low
    protected double angle;
    protected boolean moving;

    //constants
    protected double momentumMax = 3;
    protected double momentumDefault = 0.1;

    /**
    /**
     * Constructor for player character; Creates new player and assigns role  movement values
     * specific to the player character;
     * @param context - the resource context;
     * @param xRes - x axis limit derived from resolution;
     * @param yRes - y axis limit derived from resolution;
     * @param x - location in x axis;
     * @param y - location in y axis;
     * @param bitmapName - bitmap name inside /drawable/ without the extension;
     */
    public Plane(Context context, int xRes, int yRes, int x, int y, String bitmapName)
    {
        super(context, xRes, yRes, x, y, bitmapName);
        setItemRole(PLAYER);

        speed = 10.0;
        moving = false;
        angle = 0;
        momentum = momentumDefault;

    }


    //getters for speed, momentum and Bitmap
    public double getMomentum() {
        return momentum;
    }

    public boolean isMoving() {
        return moving;
    }

    /**
     * Method to adjust momentum increase;
     */
    protected void adjustMomentum() {
        if(moving)
        {
            if(momentum < momentumMax) {
                momentum = momentum * 2;
            }
            if(momentum >= momentumMax) {
                momentum = momentumMax;
            }
        }
        else {
            if(momentum > 0) {
                momentum = momentum - 0.25;
            }
            if(momentum < 0) {
                momentum = 0;
            }
        }
    }

    /**
     * Method to set the momentum to the default low value after stoping moving;
     * Is used because otherwise the characters wouldn't stop sliding around or move at all;
     */
    public void resetMomentum(){
        //resetting the momentum after stopping fully
        if(momentum == 0 && !moving)
        {
            momentum = momentumDefault;
        }
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    /**
     * Method to draw the character based on changes;
     * @param canvas - canvas visible within the GameView;
     * @param paint - current drawing tool;
     */
    @Override
    public void draw(Canvas canvas, Paint paint) {

        if (isMoving()) {
        }
        canvas.drawBitmap(bitmap, x, y, paint);
    }

    /**
     * Method to avoid dealing with very large angles and normalize then to stay within 360 degrees;
     */
    protected void normalizeAngle() {
        // reduce the angle
        angle = angle % 360;

        // force it to be the positive remainder, so that 0 <= angle < 360
        angle = (angle + 360) % 360;

        // force into the minimum absolute value residue class, so that -180 < angle <= 180
        if (angle > 180)
            angle -= 360;
    }


    /**
     * Method to move the player, update RectF hitbox values
     * @param newX - x coordinate to move away from;
     * @param newY - y coordinate to move away from;
     */
    protected void move(double newX, double newY) {

        //normalize angle before calculation
        normalizeAngle();

        //adjust the momentum
        adjustMomentum();

        //distance to cover and angle of
        double xDist, yDist;

        xDist = (newX - x) * (-1);
        yDist = (newY - y) * (-1);
        angle = atan2(yDist, xDist);

        //save old coordinates for collision adjustment


        //commit to new coordinates
        x = (float) (x + (speed * Math.cos(angle)) * momentum);
        y = (float) (y + (speed * Math.sin(angle)) * momentum);

        //check if not collided with wall
        checkWall();




        //Adding the top, left, bottom and right to the rect object
        collisionRect.left = x;
        collisionRect.top = y;
        collisionRect.right = x + xSize;
        collisionRect.bottom = y + ySize;
    }

    /**
     * Method to change position of player based on the intersection direction and passed item;
     * @param i - object that's being collided with;
     */
    public void adjustHit(Item i) {

        int collisionLocation = intersectionDirection(i);

        if(collisionLocation == 0) //left
        {
            x = i.x - xSize;
        }
        if(collisionLocation == 1) //right
        {
            x = i.x + i.xSize;
        }
        if(collisionLocation == 2) //top
        {
            y = i.y - ySize;
        }
        if(collisionLocation == 3) //bottom
        {
            y = i.y + i.ySize;
        }
        //if uncertain restore last known position
        if(collisionLocation == -1) //nothing
        {

        }
    }

    /**
     * Method for detecting direction of two RectF object intersection;
     * Compares Rect coordinates and finds the closest intersection;
     * @param obj - the object being checked for collision against a player
     * @return int ID of the side that's closest;
     */
    public int intersectionDirection(Item obj) {
        float pL = this.x,             //left
                pR = pL + this.xSize,    //right
                pT = this.y,             //top
                pB = pT + this.ySize;   //bottom

        float oL = obj.x,               //left
                oR = oL + obj.xSize,      //right
                oT = obj.y,               //top
                oB = oT + obj.ySize;     //bottom

        float array[] = new float []{1000f, 1000f, 1000f, 1000f};

        if(pR > oL && pL < oL) {             // Player on left
            array[0] = pR - oL;
        }
        if(pL < oR && pR > oR) {           // Player on Right
            array[1] = oR - pL;
        }
        if(pB > oT && pT < oT){           // Player on Bottom
            array[2] = pB - oT;
        }
        if(pT < oB && pB > oB){           // Player on Top
            array[3] = oB - pT;
        }

        // return the closest intersection
        double lowest = 1000;
        int ID = -1;
        for(int i = 0; i < array.length; i++)
        {
            if(array[i] < lowest){
                lowest = array[i];
                ID = i;
            }
        }
        return ID;
    }

}
